# Spinitron documentation

###Contents

* [SpinPapi API Specificationn](https://bitbucket.org/spinitron/documentation/src/c35d4f6c311b2a3d3a778421d59e9be330f51ec0/SpinPapi-v2.pdf?at=master) (PDF) – SpinPapi is a REST/JSON API that remote systems can use to query the Spinitron database. It is a read-only API that provides information you can use in a station web site, mobile app or things like that.

* [Metadata Push](https://bitbucket.org/spinitron/documentation/src/c35d4f6c311b2a3d3a778421d59e9be330f51ec0/Metadata-Push.pdf?at=master) (PDF) – You can configure Spinitron to send messages with information about the now-playing song, for example, to update embedded stream metadata, RDS, or Twitter. Once configured, Spinitron automatically sends updates at appropriate times. The system is flexible, supporting many protocols and arbitrary data formats.

* [Short URLs](https://bitbucket.org/spinitron/documentation/src/827efdb18104a8a838d4ae303815a3d7fd2e3adf/short-urls.md?at=master) — Description of Spinitron's short URL scheme, e.g. [http://spinitron.com/wzbc](http://spinitron.com/wzbc).

* [Automationn](https://bitbucket.org/spinitron/documentation/src/c35d4f6c311b2a3d3a778421d59e9be330f51ec0/Automation-Intro.pdf?at=master) (PDF) – User guide to integrating Spinitron with radio automation systems (introduction and TOC only). Spinitron integrates successfully with 13 different systems so far and can integrate with almost anything. Ask us about your system if it is not supported yet.

* Marketing material:

	- One-page [flyer](https://bitbucket.org/spinitron/documentation/src/c35d4f6c311b2a3d3a778421d59e9be330f51ec0/Spinitron-Flyer.pdf?at=master) (PDF)
	- Ten-page [brochure](https://bytebucket.org/spinitron/documentation/raw/7c43384fd13bfacd26c50081cb79c710a12b67ec/Spinitron-Brochure.pdf) (PDF)


### Issue tracker

The issue tracker is for the documentation here – not Spinitron in general.


### Wiki

Spinitron users, especially web devs and other technical and support people, can use the to
share ideas, tips, code, etc. For example, if you've got a nice bit of HTML and corresponding
CSS to share with other Spinitron stations, put it on the Wiki.
