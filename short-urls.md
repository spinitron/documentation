# Spinitron short URLs

Some URLs for Spinitron public pages can be shortened – useful when sharing a page. The short URLs redirect to the familiar long URLs. So Spinitron isn't changing its URL scheme, it's just adding shortcuts.

## Short station URL

To link to a station, put its station-id after http://spinitron.com/. Example

	http://spinitron.com/wzbc

expands to

	http://spinitron.com/radio/playlist.php?station=wzbc

Which is the page with the station's current playlist/show. The station-id (wzbc in the example) is what you use to log on to Spinitron and it appears after the `station=` in any normal (long) spinitron.com URL.

## Short playlist URL

To link to a playlist, put /playlist-id on the end of a short station URL. Example

	http://spinitron.com/kspc/14682

expands to

	http://spinitron.com/radio/playlist.php?station=kspc&playlist=14682

The playlist-id (14682 in the example) is the value of `playlist=` in any normal (long) playlist URL. Users of SpinPapi (the Spinitron public-data API) can find playlist-ids in various query responses.

## Other query parameters

You can add any of Spinitron's other URL parameters to either a short station or playlist URL in any of these generic forms:

	http://spinitron.com/station?param=value&param=value
	http://spinitron.com/station/?param=value&param=value
	http://spinitron.com/station/playlist?param=value&param=value

For example:

	http://spinitron.com/kafm?show=schedule                    KAFM's schedule
	http://spinitron.com/wort/?month=Aug&year=2012&day=15      WORT's 8/15/2012 playlists
	http://spinitron.com/kpov?djuid=1                          Playlists of userid 1 on KPOV
	http://spinitron.com/kzsc/?showid=1971                     Playlists of show 1971 on KZSC
	http://spinitron.com/kzsc?browse=a&l=m&pg=19               Some arists played on KZSC
	http://spinitron.com/kspc/14687?art=0                      KSPC playlist 14687 w/o cover art
